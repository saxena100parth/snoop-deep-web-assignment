package com.example.demo;



import org.springframework.data.jpa.repository.JpaRepository;

public interface CrawlerRepository extends JpaRepository<CrawlerResult, Long> {
}