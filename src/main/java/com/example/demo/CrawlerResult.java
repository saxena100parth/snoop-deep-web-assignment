package com.example.demo;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class CrawlerResult {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String seedUrl;

    private String currentUrl;

    private String html;

    private String base64Image;

    public CrawlerResult() {}

    public CrawlerResult(String seedUrl, String currentUrl, String html, String base64Image) {
        this.seedUrl = seedUrl;
        this.currentUrl = currentUrl;
        this.html = html;
        this.base64Image = base64Image;
    }


    // getters and setters


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setSeedUrl(String seedUrl) {
        this.seedUrl = seedUrl;
    }

    public String getSeedUrl() {
        return seedUrl;
    }

    public void setSeedUrl(SeedUrl seedUrl) {
        this.seedUrl = seedUrl.getUrl();
    }

    public String getCurrentUrl() {
        return currentUrl;
    }

    public void setCurrentUrl(String currentUrl) {
        this.currentUrl = currentUrl;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public String getBase64Image() {
        return base64Image;
    }

    public void setBase64Image(String base64Image) {
        this.base64Image = base64Image;
    }
}