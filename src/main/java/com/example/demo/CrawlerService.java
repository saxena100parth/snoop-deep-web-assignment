package com.example.demo;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CrawlerService {

    private final CrawlerRepository crawlerRepository;

    public CrawlerService(CrawlerRepository crawlerRepository) {
        this.crawlerRepository = crawlerRepository;
    }

    public void runCrawler(List<SeedUrl> seedUrls) {
        System.setProperty("webdriver.chrome.driver", "C:\\Windows\\chromedriver.exe");

        // Create a new ChromeDriver instance
        WebDriver driver = new ChromeDriver();

        // Set the viewport size to 1080p
        Dimension dimension = new Dimension(1920, 1080);
        driver.manage().window().setSize(dimension);

        // Loop through each seed URL and crawl the website
        for (SeedUrl seedUrl : seedUrls) {
            // Visit the seed URL
            driver.get(seedUrl.getUrl());

            // Get the current URL and HTML source code
            String currentUrl = driver.getCurrentUrl();
            String html = driver.getPageSource();

            // Take a screenshot of the current page and convert it to base64
            String base64Image;
            base64Image = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BASE64);

            // Create a new CrawlerResult object and save it to the database
            CrawlerResult result = new CrawlerResult(seedUrl.getUrl(), currentUrl, html, base64Image);
            crawlerRepository.save(result);

        }

        // Quit the ChromeDriver instance
        driver.quit();
    }

    public List<CrawlerResult> getResults() {
        // Return all CrawlerResult objects from the database
        return crawlerRepository.findAll();
    }


}
