package com.example.demo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SeedUrlRepository extends JpaRepository<SeedUrl, Long> {
}
