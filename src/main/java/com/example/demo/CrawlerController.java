package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CrawlerController {

    @Autowired
    private SeedUrlRepository seedUrlRepository;

    @Autowired
    private CrawlerService crawlerService;

    @GetMapping("/crawl")
    public void crawl() throws Exception {
        List<SeedUrl> seedUrls = seedUrlRepository.findAll();
        crawlerService.runCrawler(seedUrls);

    }

    @GetMapping("/results")
    public List<CrawlerResult> getResults() {
        return crawlerService.getResults();
    }
    @PostMapping("setSeed")
    public String setSeed(@RequestBody SeedUrl seedUrl){
        seedUrlRepository.save(seedUrl);
        return "Seed Url saved";
    }

}
