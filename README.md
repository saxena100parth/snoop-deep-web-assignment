# Snoop Deep Web ReadMe
This project is created with Java Spring Boot framework.
In this Assignment I have created three distinct API endpoints.

The first endpoint, "/crawl", is a GET API used to initiate crawling on Seed URLs that are saved in the databases.

The second endpoint, "/results", is a GET API that retrieves all of the results stored in the database after the crawling has completed.

The third endpoint, "/setSeed", is a POST API used to store Seed URLs in the database.

The database architecture of this project includes two tables.
1. The first table (SeedUrlRepository) stores all of the Seed URLs entered via the "/setSeed" API endpoint, with auto-generated IDs.
2. The second table (CrawlerRepository) is responsible for storing the results of the crawling process in the form of strings. This table contains several columns including an ID, Seed URL, Current URL, HTML, and Base64 Image.

The crawler service class contains the implementation for executing the runCrawler process and storing the output in string format into the database.

### Dependencies
The following dependencies are used in this project:

* spring-boot-starter-data-jpa
* spring-boot-starter-web
* guava
* mysql-connector
* selenium-java
* selenium-chrome-driver
* storm-crawler-core
* storm-crawler-elasticsearch
### Requirements
* Java 17
* Maven
* Build
* Chrome Driver

### Documentation/ references/ tutorials followed
1. Stormcrawler:  https://stormcrawler.net/

2. Spring boot: https://stackoverflow.com/questions/65116475/how-to-get-started-with-spring-boot
3. chrome driver with selenium: https://www.selenium.dev/documentation/

4. Apache Storm:
  https://storm.apache.org
  
Learn By Example : Apache Storm (Udemy) https://www.udemy.com/course/learn-by-example-apache-storm/#instructor-1



### Key takeaway
In this project I learn about selenium and how to integrate storowmcrawler with selenium on spring boot and how to integrate MySQl database to store the result.  

create rest APIs which include post and get mapping.


### Author
Parth saxena

Saxena100parth@gmail.com