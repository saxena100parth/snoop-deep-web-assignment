# Snoop Deep Web Assignment Deployment
This project is designed to run locally on a single machine. Once you have cloned the repository and set up the environment, you can run the application by executing the `WebprojectApplication` class.

## Environment Setup
Before running the application, ensure that you have the following software installed on your local machine:

* Java Development Kit (JDK) 11 or higher
* Apache Storm
* Selenium WebDriver
* maven 
* MySQL (I have use SQl database for storing results.)
* Spring boot (I have used spring boot as a backend framework for this assignment).
### Configuration
The application can be configured by updating the application.properties file located in the src/main/resources directory. Here are the key properties that need to be configured:

* configure sql drivers.
* configure chrome driver.

### Running the Application
To run the application, execute the executing the `WebprojectApplication` class of the project.


This will start the Spring Boot application and run the Storm topology. Once the topology has completed, the results will be stored in the configured backend database.

### Conclusion
Congratulations, you have successfully deployed and run the Snoop Deep Web application locally on your machine! If you have any issues or questions, please reach me for support.